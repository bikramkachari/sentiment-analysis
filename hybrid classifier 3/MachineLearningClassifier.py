import csv
import re
import itertools
import nltk
from nltk.collocations import TrigramCollocationFinder
from nltk.metrics import TrigramAssocMeasures
from nltk.classify.scikitlearn import SklearnClassifier
from sklearn.svm import LinearSVC
from operator import itemgetter
from copy import copy

#### Provides a Machine Learning Sentiment Analysis classifier.
class MachineLearningClassifier(object):

    def __init__(self, trainset=[]):

        # initializes a SVM classifier
        self.classifier = SklearnClassifier(LinearSVC())

        self.bag_of_words = []
        self.stopwords=self.get_stop_word_list('stopwords.rtf')

        self.classifier.probability = True
        self.train(self.classifier,trainset)
        print "MlClassifier initalized..."

    def get_stop_word_list(self,stopWordListFileName):
        stopwords=[]
        stopwords.append('AT_USER')
        stopwords.append('URL')
        fp=open(stopWordListFileName,'r')
        line=fp.readline()
        while line:
                word=line.strip()
                stopwords.append(word)
                line=fp.readline()
        fp.close()
        return stopwords
    
    def process_tweet(self,tweet):

            #remove stopwords
            for word in self.stopwords:
                if tweet.find(word)>=0:
                    tweet=tweet.replace(word,'')
            #Convert to lowercase
            tweet=tweet.lower()
            #Convert www.* or https?://* to URL
            tweet = re.sub('((www\.[\s]+)|(https?://[^\s]+))','URL',tweet)
            #Convert @username to AT_USER
            tweet = re.sub('@[^\s]+','AT_USER',tweet)
            #Remove additional white spaces
            tweet = re.sub('[\s]+', ' ', tweet)
            #Replace #word with word
            tweet = re.sub(r'#([^\s]+)', r'\1', tweet)
            #trim
            tweet = tweet.strip('\'"')
            tweet = re.sub('[.]+',' ',tweet)
            
            return tweet

    # Extract features for ML process
	
    def get_features_from_tweet(self,tweet,score_fn=TrigramAssocMeasures.chi_sq, n=10000):

        token=self.process_tweet(tweet).split()
        bigram_finder = TrigramCollocationFinder.from_words(token)
        bigrams = bigram_finder.nbest(score_fn, n)
        return dict([(ngram, True) for ngram in itertools.chain(token, bigrams)])

    def get_train_features_from_tweets(tweets,pos_neg):

            tweet_features=[]
            for tweet in tweets:
                    features=get_features_from_tweet(tweet)
                    tweet_features.append((features,pos_neg))
            return tweet_features
    


    # train the classifier
    # Tweets argument must be a list of dicitionaries. Each dictionary must
    # have the keys ['MESSAGE'] and ['SENTIMENT'] with the message string and
    # the classificationclass, respectively.
    def train(self,classifier,tweets):
        
        print "inside train..."
        self.classifier.train(tweets)


    # classify a new message. Return the scores (probabilities) for each
    # classification class
    def classify(self, tweet_message):
        
        processedTestTweet=self.process_tweet(tweet_message)
        scores=self.classifier.classify(self.get_features_from_tweet(processedTestTweet))
        #score=self.classifier.score(self.get_features_from_tweet(processedTestTweet),'positive')
        #print score
        return scores
    print "MLClassifier done....."    

