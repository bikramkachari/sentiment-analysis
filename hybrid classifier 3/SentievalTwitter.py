import csv
import re
import itertools
import nltk
from nltk.collocations import TrigramCollocationFinder
from nltk.metrics import TrigramAssocMeasures
from nltk.classify.scikitlearn import SklearnClassifier
from sklearn.svm import LinearSVC

class SentievalTwitter(object):

    # Constructor.
        def __init__(self,train_path,dev_path,test_path):
            self.train_path = train_path
            self.dev_path = dev_path
            self.test_path = test_path
            self.trainset = list()
            self.devset = list()
            self.testset = list()

            self.reader()
                    
		
		
        def get_stop_word_list(stopWordListFileName):
            stopwords=[]
            stopwords.append('AT_USER')
            stopwords.append('URL')
            fp=open(stopWordListFileName,'r')
            line=fp.readline()
            while line:
                    word=line.strip()
                    stopwords.append(word)
                    line=fp.readline()
            fp.close()
            return stopwords

        stopwords=get_stop_word_list('stopwords.rtf')

        def process_tweet(self,tweet):
            #Convert to lowercase
            tweet=tweet.lower()
            #Convert www.* or https?://* to URL
            tweet = re.sub('((www\.[\s]+)|(https?://[^\s]+))','URL',tweet)
            #Convert @username to AT_USER
            tweet = re.sub('@[^\s]+','AT_USER',tweet)
            #Remove additional white spaces
            tweet = re.sub('[\s]+', ' ', tweet)
            #Replace #word with word
            tweet = re.sub(r'#([^\s]+)', r'\1', tweet)
            #trim
            tweet = tweet.strip('\'"')
            return tweet

            # Extract features for ML process
	
        def get_features_from_tweet(self,tweet,score_fn=TrigramAssocMeasures.chi_sq, n=10000):

            token=self.process_tweet(tweet).split()
            bigram_finder = TrigramCollocationFinder.from_words(token)
            bigrams = bigram_finder.nbest(score_fn, n)
            return dict([(ngram, True) for ngram in itertools.chain(token, bigrams)])

        

	

        def reader(self):
            # For TrainSet
            pos_tweets=[]
            neg_tweets=[]
            tweets = []
            #pt = open(self.train_path,'r')
            raw_tweets=csv.reader(open(self.train_path,'rb'),delimiter=',')
            for row in raw_tweets:
                    sentiment=row[0]
                    tweet=row[1].lower()
                    item=(tweet,sentiment)
                    tweets.append(item)
                    if sentiment=="positive":
                            pos_tweets+=item
            
                    elif sentiment=="negative":
                            neg_tweets+=item
			
            neg_feats_train=self.get_train_features_from_tweets(neg_tweets,'neg')
            pos_feats_train=self.get_train_features_from_tweets(pos_tweets,'pos')	
        

            self.trainset = neg_feats_train + pos_feats_train

        def get_train_features_from_tweets(self,tweets,pos_neg):

                    tweet_features=[]
                    for tweet in tweets:
                            features=self.get_features_from_tweet(tweet)
                            tweet_features.append((features,pos_neg))
                    return tweet_features  

       

