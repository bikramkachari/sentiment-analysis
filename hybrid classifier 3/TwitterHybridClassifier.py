from MachineLearningClassifier import MachineLearningClassifier
from RulesClassifier import RulesClassifier
from LexiconClassifier import LexiconClassifier
from PreProcess import pre_process

#### Provides a hybrid Sentiment Analysis classifier.

class TwitterHybridClassifier(object):

    def __init__(self, trainset=[]):
        self.rules_classifier = RulesClassifier()
        self.lexicon_classifier = LexiconClassifier()
        self.ml_classifier = MachineLearningClassifier(trainset)

    # Apply the classifier over a tweet message in String format
    def classify(self,tweet_text):

        no_pos,no_neg,no_neu=0,0,0
        ml_tweet=tweet_text
        # 0. Pre-process the text (emoticons, misspellings, tagger)
        tweet_text = pre_process(tweet_text)

        # 1. Rule-based classifier. Look for emoticons basically
        positive_score,negative_score = self.rules_classifier.classify(tweet_text)
        rules_score = positive_score + negative_score

        # 1. Apply the rules, If any found, classify the tweet here. If none found, continue for the lexicon classifier.
        if rules_score != 0:
            if rules_score > 0:
                sentiment = 'positive'
                no_pos+=1
            else:
                sentiment = 'negative'
                no_neg+=1
            classifier_type='rb'	
            return (classifier_type,rules_score,sentiment)

        # 2. Lexicon-based classifier
        
       
                
        positive_score, negative_score = self.lexicon_classifier.classify(tweet_text)
        lexicon_score = positive_score + negative_score
        classifier_type='lb'
        #print "lexicon score: ",lexicon_score

        # 2. Apply lexicon classifier, If the lexicon score is
        # 0 (strictly neutral), >3 (positive with confidence) or
        # <3 (negative with confidence), classify the tweet here. If not,
        # continue for the SVM classifier
        if lexicon_score == 0:
            sentiment = 'neutral'
            no_neu+=1
            return (classifier_type,lexicon_score,sentiment)

        if lexicon_score >= 3:
            sentiment = 'positive'
            no_pos+=1
            return (classifier_type,lexicon_score,sentiment)

        if lexicon_score <= -3:
            sentiment = 'negative'
            no_neg+=1
            return (classifier_type,lexicon_score,sentiment)

        # 3. Machine learning based classifier - used the training set to define the best features to classify new instances
        scores = self.ml_classifier.classify(ml_tweet)
        classifier_type='ml'
        if scores=='pos':
            sentiment='positive'
        elif scores=='neg':
            sentiment='negative'
       

        return (classifier_type,scores,sentiment)

        

        
