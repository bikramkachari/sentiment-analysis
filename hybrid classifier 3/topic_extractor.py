import nltk
import re
import nltk
from nltk.corpus import brown
from string import punctuation, letters
 

 

 
 
class NPExtractor(object):
 
    def __init__(self, sentence):
        self.sentence = self.processSentence(sentence)

    def processSentence(self,tweet):
        
        tweet = re.sub('@[^\s]+','AT_USER',tweet)
        #Remove additional white spaces
        tweet = re.sub('[\s]+', ' ', tweet)
        #Replace #word with word
        tweet = re.sub(r'#([^\s]+)', r'\1', tweet)
        #trim
        tweet = tweet.strip('\'"')
        tweet = re.sub('[.]+','  ',tweet)
        #normalize words like baaaaad->bad
        for symbol in letters:
            tweet = re.sub(re.escape(symbol)+r'{3,}', symbol ,tweet)
        return tweet
 
   
 
   
 
    # Extract the main topics from the sentence
    def extract(self):
        tags=[]
        result=[]
        tags=nltk.pos_tag(nltk.word_tokenize(self.sentence))
        for word,tag in tags:
            if tag=="NN" or tag=="NNS" :
                result.append(word)
        return result        
        
 
       
 
 
